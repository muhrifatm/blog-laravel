<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;

class PertanyaanController extends Controller
{
    public function create(){
        return view('pertanyaan.create');
    }

    public function store(Request $request){
        // dd($request->all());

        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required'

            ]);

        // $query=DB::table('pertanyaan')->insert([
        //     'judul' => $request['judul'],
        //     'isi' => $request['isi'],
        // ]);

        $pertanyaan = Pertanyaan::create(
            [
                "judul" => $request["judul"],
                "isi" => $request["isi"]
            ]
            );
        return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Disimpan!');
    }

    public function index(){
        // $pertanyaan=DB::table('pertanyaan')->get();
        // dd($pertanyaan);
        $pertanyaan=Pertanyaan::all();
        return view('pertanyaan.index',compact('pertanyaan'));
    }

    public function show($id){
        //$pertanyaan= DB::table('pertanyaan')->where('id',$id)->first();
        // dd($post);
        $pertanyaan=Pertanyaan::find($id);
        return view('pertanyaan.show',compact('pertanyaan'));
    }

    public function edit($id){
        //$pertanyaan= DB::table('pertanyaan')->where('id',$id)->first();
        $pertanyaan=Pertanyaan::find($id);
        return view('pertanyaan.edit',compact('pertanyaan'));
    }

    public function update($id,Request $request){

        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required'

            ]);

        // $query= DB::table('pertanyaan')
        // ->where('id',$id)
        // ->update([
        //     'judul' => $request['judul'],
        //     'isi' => $request['isi']
        // ]);

        $update = Pertanyaan::where('id',$id)->update(
            [
                "judul"=>$request["judul"],
                "isi"=>$request["isi"]
            ]
            );
        return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Di Update!');
    }

    public function destroy($id){
        //$query= DB::table('pertanyaan')->where('id',$id)->delete();
        Pertanyaan::destroy($id);
        return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Di Delete!');
    }
}
