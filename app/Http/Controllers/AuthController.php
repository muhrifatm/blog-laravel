<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }

    public function welcome(Request $request){
        $firstName = $request->firstName;
        $lastName = $request->lastName;
        $gender = $request->gender;
        $nationality = $request->nationality;
        $bio = $request->bio;

        return view('welcome2',compact('firstName','lastName','gender','nationality','bio'));
    }
}
