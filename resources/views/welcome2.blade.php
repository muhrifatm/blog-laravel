<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome</title>
</head>
<body>
    <h1>Selamat Datang {{$firstName}}</h1>
    <h2>Terimakasih telah datang di Sanberbook. Sosial Media Bersama Kita</h2>
    <h3>Ini dia data anda:</h3>
    <ul>
        <li>Nama depan: {{$firstName}}</li>
        <li>Nama belakang: {{$lastName}}</li>
        <li>Gender: {{$gender}}</li>
        <li>Bio: {{$bio}}</li>
    </ul>
</body>
</html>