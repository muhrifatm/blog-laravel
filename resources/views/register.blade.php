<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
    <h1>Buat Account Baru</h1>

    <h3>Sign Up Form!</h3>

    <form action="/welcome" method="POST">
        @csrf
        <p>First Name:</p>
        <input type="text" name="firstName">

        <p>Last Name:</p>
        <input type="text" name="lastName"> 

        <p>Gender:</p>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">Other</label>

        <p>Nationality</p>
        <select name="nationality" id="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Singaporean">Singaporean</option>
        </select>

        <p>Bio</p>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
        <br>
        <button type="submit" value="Sign Up">Sign Up</button>
    </form>

</body>
</html>